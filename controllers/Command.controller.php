<?php

class CommandController {
    function __construct() {
        
    }

    public static function get($method = null){
        switch ($method) {
            case 'getPacientList':
                $pacientList = new PacientList();
                $result = $pacientList->getPacientList();
                General::ViewFetch('views/pacient-list.html', $result); 
            break;
            case 'getPacient':
                $pacient = new Pacient();
                $result = $pacient->getPacient(); 
                //$result['numenclature'] = General::getOperationsCodes();
                General::ViewFetch('views/pacient/pacient.html', $result);
            break;
            case 'saveOperation':
                $pacient = new Pacient();
                if (isset($_POST['pacient-id'])) {
                    $vars = array();
                    $vars['id'] = $_POST['pacient-id'];
                    $vars['oper_date'] = $_POST['operation-date'];
                    $vars['oper_code'] = $_POST['operations-code'];
                    $vars['complication_code'] = $_POST['complication-code'];
                    $result = $pacient->saveOperation($vars);
                }
            break;
            case 'saveStatistic':
                $pacient = new Pacient();
                if (isset($_POST['pacient-id'])) {
                    $vars = array();
                    $vars['id'] = $_POST['pacient-id'];
                    $vars['bed_days'] = $_POST['bed_days'];
                    $vars['bed_days_before_oper'] = $_POST['bed_days_before_oper'];
                    $vars['bed_days_after_oper'] = $_POST['bed_days_after_oper'];
                    $vars['pensioner'] = $_POST['pensioner'];
                    $vars['country'] = $_POST['country'];
                    $vars['extr'] = $_POST['extr'];
                    $vars['ssp'] = $_POST['ssp'];
                    $vars['trauma'] = $_POST['trauma'];
                    $vars['ren'] = $_POST['ren'];
                    $vars['vmp'] = $_POST['vmp'];

                    $result = $pacient->saveStatistic($vars);
                }
            break;
            case 'deleteStatistic':
                $pacient = new Pacient();
                if (isset($_POST['idzap'])) {
                    $result = $pacient->deleteStatistic($_POST['idzap']);
                }
            break;
            case 'deleteOperation':
                $pacient = new Pacient();
                if (isset($_POST['idzap'])) {
                    $result = $pacient->deleteOperation($_POST['idzap']);
                }
            break;
            case 'getOperationNumenclature':
                $result = General::getOperationsCodes($_POST['query']);
                $html = '<table id="operationNumList" class="table-hover">';
                foreach ($result as $row) {
                    $id = $row['ID'];
                    $name = $row['NAME'];
                    $string = $row['ID'] . ': ' . $row['NAME'];
                    $html .= "<tr><td data-id='$id'>$string</td></tr>";
                }
                $html .= '</table>';
                print $html;
            break;
            default: print 'default';
        }
    }
}
