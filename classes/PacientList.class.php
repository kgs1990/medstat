<?php

class PacientList {

    function __construct() {
        
    }

    public function getPacientList() {
        $top = 1000;
        $search = '';

        if (isset($_GET['top'])) {
            $top = $_GET['top'];
        };

        if (isset($_POST['search-value'])) {
            $search = $_POST['search-value'];
        };

        $query = 'SELECT TOP ' . $top . '
                ном_истор as id, 
                номерок as history, 
                фио as fio, 
                fam, 
                im, 
                ot, 
                дата_рожд as birthDate, 
                пол as gender, 
                возраст as age,
                pensioner 
            FROM ПРИЕМ '
            . 'WHERE фио LIKE \'%' . $search .'%\' OR номерок LIKE \'%' . $search .'%\' '
            . 'ORDER BY ном_истор DESC';
        $result = sqlsrv_query(Connection::$link, $query);
        
        $array = array(); // хранилище для данных
    
        // перебираем данные и сохраняем в массив
        while($pacient = sqlsrv_fetch_array($result, SQLSRV_FETCH_ASSOC))
        {   
            $pacientData = new Pacient();
            $pacientMoving = $pacientData->getPacientMove($pacient['id']);

            $pacient['date_post'] = $pacientMoving[0]['date_post']; // в списке всегда берём дату поступления в учреждение (т.е. первую)
            
            if (isset($pacientMoving[0]['year'])) {
                $pacient['year'] = $pacientMoving[0]['year'];
            } else {
                $pacient['year'] = '---';
            }
                
            // если есть переводы, то берём текуще отделение и последнюю дату выписки
            if (count($pacientMoving) > 1) {
                $currentNode = count($pacientMoving) - 1;
                $pacient['date_vyp'] = $pacientMoving[$currentNode]['date_vyp'];
                $pacient['otd'] = $pacientMoving[$currentNode]['otd'];
            } else {
                $pacient['otd'] = $pacientMoving[0]['otd'];
                $pacient['date_vyp'] = $pacientMoving[0]['date_vyp'];
            }

            $pacient['ds'] = isset($pacientMoving[0]['ds']) ? $pacientMoving[0]['ds'] : '' ;

            array_push($array, $pacient);
        }

        return $array;
    }
}
