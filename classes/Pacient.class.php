<?php

class Pacient {
    function __construct() {
        
    }

    public function getPacient() {
        if (!isset($_GET['pacientId'])) return false;

        $query = 'SELECT 
                ном_истор as id, 
                номерок as history, 
                фио as fio, 
                fam, 
                im, 
                ot, 
                дата_рожд as birthDate, 
                пол as gender, 
                возраст as age
            FROM ПРИЕМ
            WHERE ном_истор = ' . $_GET['pacientId'];

        $result = sqlsrv_query(Connection::$link, $query);

        $array = array(); // хранилище для данных

        // перебираем данные и сохраняем в массив
        while($pacient = sqlsrv_fetch_array($result, SQLSRV_FETCH_ASSOC))
        {   
            if (!empty($pacient['birthDate'])) {
                $pacient['birthDate'] = $pacient['birthDate']->format('d-m-Y');
            } else {
                $pacient['birthDate'] = 'n/a';
            }

            $pacient['moving'] = $this->getPacientMove($pacient['id']);
            $operations = $this->getOperations($pacient['id']);
            $pacient['operationsDetails'] = $operations['operationsDetails'];
            $pacient['operationsStatistic'] = $operations['operationsStatistic'];
            $pacient['statistic'] = $this->getStatisticByPacient($pacient['id']);
            
            array_push($array, $pacient);
        }

        return $array;
    }

    public function getPacientIdByName($vars) {
        $query = 'SELECT ном_истор as id
            FROM ПРИЕМ
            WHERE фио LIKE \'' . $vars['name'] . '\' AND номерок = \'' . $vars['history'] . '\'';

        $result = sqlsrv_query(Connection::$link, $query);

        if ($result) {
            $array = array(); // хранилище для данных

            // перебираем данные и сохраняем в массив
            while($pacient = sqlsrv_fetch_array($result, SQLSRV_FETCH_ASSOC))
            {   
                array_push($array, $pacient);
            }

            return $array;

        } else {

            return false;

        }
    }

    public function getPacientMove($pacientId) {
        $query = 'SELECT 
                отдледение as otd,
                дата_пост as date_post,
                дата_вып as date_vyp,
                год as year,
                ds
            FROM ПЕРЕВОДЫ
                INNER JOIN СП_отделений ON ПЕРЕВОДЫ.код_отделения = СП_отделений.код_отделения
            WHERE номер_истории = ' . $pacientId;

        $result = sqlsrv_query(Connection::$link, $query);

        $array = array(); // хранилище для данных
        
        // перебираем данные и сохраняем в массив
        while($move = sqlsrv_fetch_array($result, SQLSRV_FETCH_ASSOC))
        {
            if (isset($move['date_post']) && !empty($move['date_post'])) {
                $move['date_post'] = $move['date_post']->format('d-m-Y');
            } else {
                $move['date_post'] = '---';
            }

            if (!empty($move['date_vyp'])) {
                $move['date_vyp'] = $move['date_vyp']->format('d-m-Y');
            } else {
                $move['date_vyp'] = '---';
            }
            
            array_push($array, $move);
        }

        (count($array) == 0) ? $array[0] = array('otd' => '---', 'date_post' => '---', 'date_vyp' => '---') : '';

        return $array;
    }

    private function getOperations($pacientId) {
        
        $array = array(); // хранилище для данных

        try {
            $query = 'SELECT id_zap, oper_date, oper_code, complicate, [NAME]
            FROM [medstat.oper] LEFT JOIN [medstat.numenclature] ON [medstat.oper].[oper_code] = [medstat.numenclature].[ID]
            WHERE id_pac = ' . $pacientId;

            $result = sqlsrv_query(Connection::$link, $query);

            $array['operationsDetails'] = array();

            // перебираем данные и сохраняем в массив
            while($operation = sqlsrv_fetch_array($result, SQLSRV_FETCH_ASSOC))
            {
                if (!empty($operation['oper_date'])) {
                    $operation['oper_date'] = $operation['oper_date']->format('d-m-Y');
                } else {
                    $operation['oper_date'] = '---';
                }
                 
                array_push($array['operationsDetails'], $operation);
            }
        } catch(Exception $e) {

        }
        
        try {
            $query = 'SELECT COUNT(id_pac) AS count, oper_code
            FROM [medstat.oper]
            WHERE id_pac = ' . $pacientId .'
            GROUP BY oper_code';

            $result = sqlsrv_query(Connection::$link, $query);

            if ($result) {
                $array['operationsStatistic'] = array();

                while($statistic = sqlsrv_fetch_array($result, SQLSRV_FETCH_ASSOC))
                {
                    array_push($array['operationsStatistic'], $statistic);
                }
            }
        } catch(Exception $e) {

        }
        
        return $array;
    }

    public function getStatisticByPacient($pacientId) {
        $query = 'SELECT id_zap, bed_days, bed_days_before_oper, bed_days_after_oper, pensioner, country, extr, ssp, trauma, ren, vmp
            FROM [medstat.statistic]
            WHERE id_pac = ' . $pacientId;

        $result = sqlsrv_query(Connection::$link, $query);

        if ($result) {
            $array = array();

            $info = '';

            while($statistic = sqlsrv_fetch_array($result, SQLSRV_FETCH_ASSOC))
            {
                $info .= $statistic['pensioner'] ?  'Пенсионер; ' : '';
                $info .= $statistic['country'] ?  'Сельский; ' : '';
                $info .= $statistic['extr'] ?  'Экстренный; ' : '';
                $info .= $statistic['ssp'] ?  'ССП; ' : '';
                $info .= $statistic['trauma'] ?  'Травма; ' : '';
                $info .= $statistic['ren'] ?  'РХМДиЛ; ' : '';
                $info .= $statistic['vmp'] ?  'ВМП; ' : '';

                $statistic['info'] = $info;

                array_push($array, $statistic);
            }

            return $array;
        }
    }

    public function saveOperation($vars) {
        $id = $vars['id'];
        $operDate = $vars['oper_date'];
        $operCode = $vars['oper_code'];
        $complication = $vars['complication_code'];

        $query = "INSERT INTO [medstat.oper](id_pac, oper_date, oper_code, complicate)
            VALUES ($id, '$operDate', '$operCode', '$complication')";

        $result = sqlsrv_query(Connection::$link, $query);
    }

    public function deleteOperation($idZap) {
        $query = "DELETE FROM [medstat.oper] WHERE id_zap = " . $idZap;
        $result = sqlsrv_query(Connection::$link, $query);
    }

    public function saveStatistic($vars) {
        $id = $vars['id'];
        $bedDays = $vars['bed_days'];
        $bedDaysBefore = $vars['bed_days_before_oper'];
        $bedDaysAfter = $vars['bed_days_after_oper'];

        $pensioner = !empty($vars['pensioner']) ? 1 : 0;
        $country = !empty($vars['country']) ? 1 : 0;
        $extr = !empty($vars['extr']) ? 1 : 0;
        $ssp = !empty($vars['ssp']) ? 1 : 0;
        $trauma = !empty($vars['trauma']) ? 1 : 0;
        $ren = !empty($vars['ren']) ? 1 : 0;
        $vmp = !empty($vars['vmp']) ? 1 : 0;

        $query = "INSERT INTO [medstat.statistic](id_pac, bed_days, bed_days_before_oper, bed_days_after_oper, pensioner, country, extr, ssp, trauma, ren, vmp)
            VALUES ($id, '$bedDays', '$bedDaysBefore', '$bedDaysAfter', '$pensioner', '$country', '$extr', '$ssp', '$trauma', '$ren', '$vmp')";

        $result = sqlsrv_query(Connection::$link, $query);
    }

    public function deleteStatistic($idZap) {
        $query = "DELETE FROM [medstat.statistic] WHERE id_zap = " . $idZap;
        $result = sqlsrv_query(Connection::$link, $query);
    }
}