<?php

class Connection {
    public static $link;

    function __construct() {

    }

    public static function Connect() {
        if (self::$link) return true;
        if (!defined('DB_HOST')) return false;
        if (!defined('DB_USER')) return false;
        if (!defined('DB_PASS')) return false;
        if (!defined('DB_BASE')) return false;

        $connectInfo = array("uid" => DB_USER, "PWD" => DB_PASS, "Database"=> DB_BASE, "CharacterSet" => USE_UTF8);
        
        self::$link = sqlsrv_connect( DB_HOST, $connectInfo);

        if (!self::$link) return false;

        return true;
    }
}
