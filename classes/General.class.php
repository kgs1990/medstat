<?

class General {
    public static function ViewFetch ($view = null, $data = null) {
        if (empty($view)) return false;

        require ($view);
    }

    public static function getOperationsCodes($string = ''){
        $query = "SELECT [ID], [NAME] FROM [medstat.numenclature] WHERE [ID] LIKE  '%" . $string ."%' OR [NAME] LIKE '%" . $string ."%'";
        $result = sqlsrv_query(Connection::$link, $query);
        
        if ($result) {
            $array = array();

            while($numenclature = sqlsrv_fetch_array($result, SQLSRV_FETCH_ASSOC)) {
                array_push($array, $numenclature);
            }

            return $array;
        }
    }
}
