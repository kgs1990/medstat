$('#saveOperation').on('click', function(event){
    $.ajax({
		url: '/medstat/?cmd=saveOperation',
		method: 'post',
		data: $('#operationForm').serialize(),
		success: function(data){
			//$('#closeOperationForm').click();
			location.reload();
			return false;
		}
	});
});

$('#saveStatistic').on('click', function(event){
    $.ajax({
		url: '/medstat/?cmd=saveStatistic',
		method: 'post',
		data: $('#statisticForm').serialize(),
		success: function(data){
			location.reload();
			return false;
		}
	});
});

$('.js-delete-statistic').on('click', function(event) {
	event.preventDefault();
	let btn = $(this);

	$.ajax({
		url: '/medstat/?cmd=deleteStatistic',
		method: 'post',
		data: 'idzap=' + btn.data('idzap'),
		success: function(data) {
			location.reload();
			return false;
		}
	});
});

$('.js-delete-operation').on('click', function(event) {
	event.preventDefault();
	let btn = $(this);

	$.ajax({
		url: '/medstat/?cmd=deleteOperation',
		method: 'post',
		data: 'idzap=' + btn.data('idzap'),
		success: function(data) {
			location.reload();
			return false;
		}
	});
});

$('#findOperNum').on('click', function(e){
	e.preventDefault();
	let container = $('#operationContainter');
	container.html('');
	$.ajax({
		url: '/medstat/?cmd=getOperationNumenclature',
		method: 'post',
		data: 'get=data&query=' + $('#operations-code-search').val(),
		success: function(data){
			container.css('display', 'block');
			container.html(data);
			
		},
		error: function(data) {
			container.css('display', 'none');
			console.log(data);
		}
	})
});

$('#operationContainter').on('click', "td", function(e) {
	e.preventDefault();
	let trow = $(this).closest('tr');
	let table = $(this).closest('table');
	table.find('tr').css('background-color', '#FFFFFF');
	trow.css('background-color', '#ADFF2F');
	$('#operations-code').val($(this).data('id'));
});