<?php
include './config.php';
include './assets.php';
require_once ('classes/PHPExcel.php');

Connection::Connect();

$xls = PHPExcel_IOFactory::load('uploads/oper.xls');

$lists = array();

foreach($xls->getWorksheetIterator() as $worksheet) {
    $lists[] = $worksheet->toArray();
}

$pages = $xls->getSheetCount();

/*for ($i = 0; $i < $pages; $i++) {

}*/

/*
$row data[]
2 - номер истории
3 - фио
4 - дата поступления
5 - дата выписки
*/

?>

<table>
<?php $pacient = new Pacient(); ?>
<?php foreach ($lists[0] as $row) { 
    if (empty($row[2])  && empty($row[3])) continue;
    $vars = array();
    $vars['history'] = $row[2];
    $vars['name'] = $row[3];
    $result = $pacient->getPacientIdByName($vars);
    if (isset($result[0]['id'])) $vars['id'] = $result[0]['id'];
?>
    <tr>
        <td><?= $vars['id'] ?></td>
        <td><?= $row[2] ?></td>
        <td><?= $vars['name'] ?></td>
        <td><?= $row[4] ?></td>
        <td><?= $row[5] ?></td>
    </tr>
<?php if($row[2] == '36673') break; } ?>
</table>