<?php

    include 'config.php';
    include 'assets.php';

    Connection::Connect();

    if (!empty($_GET['cmd'])) {
        $method = $_GET['cmd'];
    } else {
        $method = 'default'; 
    }

    if (!empty($_POST['get']) && ($_POST['get'] == 'data')) {
        CommandController::get($method);
    } else {
        require_once('views/header/header.html');

        CommandController::get($method);

        require_once('views/footer/footer.html');
    }

    
    